#include <iostream>
#include <cmath>
#include <vector>
using namespace std;

class Fraction
{
public:
	Fraction(int numerator, int denominator);
private:
	int numerator;
	int denominator;
};

Fraction::Fraction(int numerator, int denominator)
{
	if (denominator == 0)
		throw std::runtime_error("denominator equal 0");

	this->numerator = numerator;
	this->denominator = denominator;
}

void fractionTest();

int main(int argc, char** argv)
{
	fractionTest();
}


void fractionTest()
{
	std::cout << "enter nomenator of fraction \n";
	int nomenator;
	std::cin >> nomenator;
	std::cout << "enter denomerator of fraction \n";
	int denomerator;
	std::cin >> denomerator;

	Fraction* f1 = nullptr;
	try
	{
		f1 = new Fraction(nomenator, denomerator);
	}
	catch (const std::exception& exception)
	{
		std::cerr << "can't make fraction: " << exception.what();
	}

	if (f1 != nullptr)
		delete f1;
}
